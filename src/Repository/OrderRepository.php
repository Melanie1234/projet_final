<?php

namespace App\Repository;
use App\Entity\Order;
use DateTime;
use PDO;


class OrderRepository {
    private PDO $connection;
    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }
    public function sqlToOrder(array $line): Order
    {
       
        $date = null;
        if (isset($line['date'])) {
            $date = new DateTime($line['date']);
        }
        

        return new Order($date, $line['deliveryMode'], $line['status'], $line['user_id'], $line['book_id'], $line['id']);
    }

    public function findAll():array {
        $orders = [];
        $query = $this->connection->prepare('SELECT * FROM `order`');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $item) {
            $order = $this->sqlToOrder($item);
         
            $orders[] = $order;
        }

        return $orders;
    }

    public function persist(Order $order): void
    {

        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO `order` (date, deliveryMode, status, user_id, book_id) VALUES 
        (:date, :deliveryMode, :status, :user_id, :book_id)');
        $query->bindValue(':date', $order->getDate()->format('Y-m-d'));
        $query->bindValue(':deliveryMode', $order->getDeliveryMode());
        $query->bindValue(':status', $order->getStatus());
        $query->bindValue(':user_id', $order->getUserId());
        $query->bindValue(':book_id', $order->getBookId());
        
        $query->execute();
        $order->setId($connection->lastInsertId());
    }

    public function findById(int $id): ?Order
    {
        $statement = $this->connection->prepare('SELECT * FROM `order` WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            $order = $this-> sqlToOrder($result);

            return $order;
        }
        return null;
    }

    public function delete(Order $order)
    {
        $statement = $this->connection->prepare('DELETE  FROM `order` WHERE id = :id');
        $statement->bindValue('id', $order->getId(), PDO::PARAM_INT);
        $statement->execute();
    }
    
}
