<?php

namespace App\Repository;
use App\Entity\Book;
use PDO;

class BookRepository {
    private PDO $connection;
    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    public function sqlToBook(array $line): Book
    {
        return new Book(
        $line['isbn_13'], 
        $line['publishers'], 
        $line['title'], 
        $line['genres'], 
        $line['number_of_pages'], 
        $line['publish_date'], 
        $line['author'], 
        $line['comment'], 
        $line['img'], 
        $line['score'], 
        $line['user_id'], 
        $line['id'], 
        );
    }


    public function findAll(): array
    {
        $books = [];
        $query = $this->connection->prepare(
            'SELECT book.* FROM book LEFT JOIN `order` 
            on book.id=`order`.book_id where `order`.id IS NULL;'
        );
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $item) {
            $book = $this->sqlToBook($item);
            $books[] = $book;
        }
        return $books;
    }
    public function persist(Book $book): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO book (isbn_13, publishers, title, genres, 
        number_of_pages, publish_date, author, comment, img, score, user_id) 
        VALUES (:isbn13, :publishers, :title, :genres, :numberOfPages, :publishDate, 
        :author, :comment, :img, :score, :user_id)');
        
        $query->bindValue(':isbn13', $book->getIsbn13());
        $query->bindValue(':publishers', $book->getPublishers());
        $query->bindValue(':title', $book->getTitle());
        $query->bindValue(':genres', $book->getGenres());
        $query->bindValue(':numberOfPages', $book->getNumberOfPages());
        $query->bindValue(':publishDate', $book->getPublishDate());
        $query->bindValue(':author', $book->getAuthor());
        $query->bindValue(':comment', $book->getComment());
        $query->bindValue(':img', $book->getImg());
        $query->bindValue(':score', $book->getScore());
        $query->bindValue(':user_id', $book->getUserId());

        $query->execute();
        $book->setId($connection->lastInsertId());
    }

    public function findById(int $id): ?Book
    {
        $statement = $this->connection->prepare('SELECT * FROM book WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            $book = $this-> sqlToBook($result);

            return $book;
        }
        return null;
    }

    public function findByIsbn(string $isbn): ?Book
    {
        $statement = $this->connection->prepare('SELECT * FROM book WHERE isbn_13=:isbn_13');
        $statement->bindValue('isbn_13', $isbn);
        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            $book = $this-> sqlToBook($result);

            return $book;
        }
        return null;
    }
    public function findByUserId(int $userId): array
    {
        $books = [];
        $statement = $this->connection->prepare('SELECT * FROM book WHERE user_id=:user_id');
        $statement->bindValue('user_id', $userId);
        $statement->execute();
        $result = $statement->fetchAll();
        foreach ($result as $item) {
            $book = $this->sqlToBook($item);
            $books[] = $book;
        }
        return $books;
    }
    

    public function searchTitleOrAuthor(string $query): array {
        $query = '%' . $query . '%'; 
    
        $statement = $this->connection->prepare('SELECT * FROM book WHERE title LIKE :query OR author LIKE :query');
        $statement->bindValue('query', $query, PDO::PARAM_STR);
        $statement->execute();
    
        $results = $statement->fetchAll();
    
        $books = [];
    
        foreach ($results as $item) {
            $book = $this->sqlToBook($item);
            $books[] = $book;
        }
    
        return $books;
    }
    
    public function delete(Book $book)
    {
        $statement = $this->connection->prepare('DELETE  FROM book WHERE id = :id');
        $statement->bindValue('id', $book->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

}


