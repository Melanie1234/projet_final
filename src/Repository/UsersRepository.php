<?php

namespace App\Repository;

use App\Entity\Users;
use PDO;

class UsersRepository
{
    private PDO $connection;

    public function __construct()
    {
        $this->connection = Connection::getConnection();
    }

    public function sqlToUser(array $line): Users
    {
        return new Users(firstName: $line['firstName'], lastName: $line['lastName'], mail: $line['mail'], address: $line['address'], department: $line['department'], city: $line['city'], role: $line['role'], password: $line['password'], score: $line['score'], id: $line['id']);
    }

    public function findAll(): array
    {
        $users = [];
        $query = $this->connection->prepare('SELECT * FROM users');
        $query->execute();
        $result = $query->fetchAll();

        foreach ($result as $item) {
            $user = $this->sqlToUser($item);
            $users[] = $user;
        }
        return $users;
    }

    public function persist(Users $users): void
    {
        $connection = Connection::getConnection();
        $query = $connection->prepare('INSERT INTO users (firstName, lastName, mail, address, department, city, role, password, score) VALUES 
        (:firstName,:lastName,:mail, :address, :department, :city, :role, :password, :score)');
        $query->bindValue(':firstName', $users->getFirstName());
        $query->bindValue(':lastName', $users->getLastName());
        $query->bindValue(':mail', $users->getMail());
        $query->bindValue(':address', $users->getAddress());
        $query->bindValue(':department', $users->getDepartment());
        $query->bindValue(':city', $users->getCity());
        $query->bindValue(':role', $users->getRole());
        $query->bindValue(':password', $users->getPassword());
        $query->bindValue(':score', $users->getScore());
        $query->execute();
        $users->setId($connection->lastInsertId());
    }

    public function findById(int $id): ?Users
    {
        $statement = $this->connection->prepare('SELECT * FROM users WHERE id=:id');
        $statement->bindValue('id', $id);
        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            $user = $this->sqlToUser($result);

            return $user;
        }
        return null;
    }

    public function findByMail(string $mail): ?Users
    {
        $statement = $this->connection->prepare('SELECT * FROM users WHERE mail=:mail');
        $statement->bindValue(':mail', $mail);
        $statement->execute();

        $result = $statement->fetch();
        if ($result) {
            $user = $this->sqlToUser($result);

            return $user;
        }
        return null;
    }



    public function delete(Users $user)
    {
        $statement = $this->connection->prepare('DELETE  FROM users WHERE id = :id');
        $statement->bindValue('id', $user->getId(), PDO::PARAM_INT);
        $statement->execute();
    }

    public function update(Users $user): void
    {
        $query = 'UPDATE users SET 
              firstName = :firstName,
              lastName = :lastName,
              mail = :mail,
              address = :address,
              department = :department,
              city = :city,
              role = :role,
              password = :password,
              score = :score
              WHERE id = :id';

        $statement = $this->connection->prepare($query);
        $statement->bindValue(':firstName', $user->getFirstName());
        $statement->bindValue(':lastName', $user->getLastName());
        $statement->bindValue(':mail', $user->getMail());
        $statement->bindValue(':address', $user->getAddress());
        $statement->bindValue(':department', $user->getDepartment());
        $statement->bindValue(':city', $user->getCity());
        $statement->bindValue(':role', $user->getRole());
        $statement->bindValue(':password', $user->getPassword());
        $statement->bindValue(':score', $user->getScore());
        $statement->bindValue(':id', $user->getId(), PDO::PARAM_INT);

        $statement->execute();
    }

}