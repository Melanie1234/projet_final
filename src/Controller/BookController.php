<?php

namespace App\Controller;


use App\Entity\Book;
use App\Entity\User;
use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/book')]
class BookController extends AbstractController
{
    private BookRepository $repo;
    public function __construct(BookRepository $repo)
    {
        $this->repo = $repo;
    }

    #[Route(methods: 'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

 

    #[Route(methods: 'POST')]
public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
{
    try {
        $book = $serializer->deserialize($request->getContent(), Book::class, 'json');
    } catch (\Exception $e) {
        return $this->json($e, 400);
    }

    $errors = $validator->validate($book);
    if ($errors->count() > 0) {
        return $this->json($errors, 400);
    }
    
        /**
         * @var User
         */
        $user = $this->getUser();
        
  
    $book->setUserId($user->getId());
    $book->setScore($book->getScore() + 5);
    $this->repo->persist($book);
    return $this->json($book, 201);
}

    #[Route('/{isbn13}', methods: 'GET')]
    public function oneIsbn(string $isbn13)
    {
        $book = $this->repo->findByIsbn($isbn13);
        if (!$book) {
            throw new NotFoundHttpException();
        }
        return $this->json($book);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $book = $this->repo->findById($id);
        if (!$book) {
            throw new NotFoundHttpException();
        }
        return $this->json($book);
    }

    
    #[Route('/user/books', methods: 'GET')]
    public function oneUser()
    {
        /**
         * @var User
         */
        $user = $this->getUser();
        $books = $this->repo->findByUserId($user->getId());
        if (!$books) {
           return $this->json(['no result']);
        }
        return $this->json($books);
    }


    #[Route('/search/{query}', methods: 'GET')]
    public function search(string $query)
    {
        $matchingBooks = $this->repo->searchTitleOrAuthor($query);

        if (empty($matchingBooks)) {
            return $this->json(['message' => 'No results']);
        }

        return $this->json($matchingBooks);
    }


    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Request $request, SerializerInterface $serializer, int $id)
    {

        $book = $this->repo->findById($id);
        if (!$book) {
            return $this->json(['message' => 'Book not found'], Response::HTTP_NOT_FOUND);
        }
        $this->repo->delete($book);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}