<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/user')]
class UserController extends AbstractController
{
    public function __construct(private UsersRepository $repo) {}

    #[Route('/admin', methods:'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

   

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $user = $this->repo->findById($id);
        if (!$user) {
            throw new NotFoundHttpException();
        }
        return $this->json($user);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Request $request, SerializerInterface $serializer, int $id)
    {

        $user = $this->repo->findById($id);
        if (!$user) {
            return $this->json(['message' => 'User not found'], Response::HTTP_NOT_FOUND);
        }
        $this->repo->delete($user);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }

    #[Route('/{id}', methods: 'PATCH')]
    public function update(Request $request, SerializerInterface $serializer, int $id): JsonResponse
    {
        $user = $this->repo->findById($id);
        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        $requestData = json_decode($request->getContent(), true);

        if (isset($requestData['firstName'])) {
            $user->setFirstName($requestData['firstName']);
        }

        if (isset($requestData['lastName'])) {
            $user->setLastName($requestData['lastName']);
        }

        if (isset($requestData['mail'])) {
            $user->setMail($requestData['mail']);
        }

        if (isset($requestData['adress'])) {
            $user->setAddress($requestData['adress']);
        }

        if (isset($requestData['department'])) {
            $user->setDepartment($requestData['department']);
        }

        if (isset($requestData['city'])) {
            $user->setCity($requestData['city']);
        }

        $this->repo->update($user);

        return $this->json($user, Response::HTTP_OK);
    }

    
}