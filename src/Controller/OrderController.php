<?php
namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

#[Route('/api/order')]
class OrderController extends AbstractController
{
    public function __construct(private OrderRepository $repo) {}

    #[Route(methods:'GET')]
    public function all(): JsonResponse
    {
        return $this->json($this->repo->findAll());
    }

    #[Route(methods:'POST')]
    public function add(Request $request, SerializerInterface $serializer, ValidatorInterface $validator): JsonResponse
    {
        try{
            $order = $serializer->deserialize($request->getContent(), Order::class, 'json');
        }catch(\Exception $e) {
            
            return $this->json('Invalid body', 400);
        }

        $errors = $validator->validate($order);
        if($errors->count() > 0) {
            return $this->json($errors, 400);
        }
        $this->repo->persist($order);
        return $this->json($order, 201);
    }

    #[Route('/{id}', methods: 'GET')]
    public function one(int $id)
    {
        $order = $this->repo->findById($id);
        if (!$order) {
            throw new NotFoundHttpException();
        }
        return $this->json($order);
    }

    #[Route('/{id}', methods: 'DELETE')]
    public function delete(Request $request, SerializerInterface $serializer, int $id)
    {

        $order = $this->repo->findById($id);
        if (!$order) {
            return $this->json(['message' => 'Order not found'], Response::HTTP_NOT_FOUND);
        }
        $this->repo->delete($order);
        return $this->json(null, Response::HTTP_NO_CONTENT);
    }
}
