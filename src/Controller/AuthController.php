<?php

namespace App\Controller;

use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Exception\ValidationFailedException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AuthController extends AbstractController
{
    private UsersRepository $repo;
    public function __construct(usersRepository $repo) {}

   

    #[Route('/api/user', methods: 'POST')]
    public function index(UsersRepository $repo, Request $request, SerializerInterface $serializer, 
    UserPasswordHasherInterface $hasher, ValidatorInterface $validator): JsonResponse
    {
        try {
            $user = $serializer->deserialize($request->getContent(), Users::class, 'json');

        } catch (\Exception $e) {
            return $this->json('Invalid body', 400);
        }

        $errors = $validator->validate($user);
        if($errors->count() > 0) {
            return $this->json(['errors' => $errors], 400);
        }

        if ($repo->findByMail($user->getMail())) {
            return $this->json('User Already exists', 400);
        }

        $hash = $hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($hash);
        $user->setRole('ROLE_USER'); 

        $repo->persist($user);

        return $this->json($user, 201);
    }

    #[Route('/api/account', methods: 'GET')]
    public function protectedRoute() {
        return $this->json($this->getUser());
    }

    #[Route('/api/protected', methods: 'DELETE')]
    public function deleteUser()
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->json(['error' => 'User not found'], Response::HTTP_NOT_FOUND);
        }

        try {
            $this->repo->delete($user);
            return $this->json(['message' => 'User deleted successfully']);
        } catch (\Exception $e) {
            return $this->json(['error' => 'Failed to delete user'], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/api/protected', methods: 'PUT')]

    public function updateUser(Request $request, SerializerInterface $serializer)
    {
        try {
            $user = $this->getUser();
            $serializer->deserialize($request->getContent(), User::class, 'json', [
                'object_to_populate' => $user,
                'ignored_attributes' => [
                    'password',
                    'role'
                ]
            ]);
            $this->repo->persist($user);
            return $this->json($user);

        } catch (ValidationFailedException $e) {
            return $this->json($e->getViolations(), Response::HTTP_BAD_REQUEST);
        } catch (NotEncodableValueException $e) {
            return $this->json('Invalid json', Response::HTTP_BAD_REQUEST);
        }
    }
}