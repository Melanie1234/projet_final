<?php

namespace App\Security;
use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface {

    public function __construct(private UsersRepository $repo){}

    public function loadUserByIdentifier(string $identifier):UserInterface {
        $user = $this->repo->findByMail($identifier);
        if($user == null) {
            throw new UserNotFoundException();
        }
        return $user;
    }

    public function refreshUser(UserInterface $userInterface):UserInterface {
        return $this->loadUserByIdentifier($userInterface->getUserIdentifier());
    }

    public function supportsClass(string $class):bool {
        return $class == Users::class;
    }
}