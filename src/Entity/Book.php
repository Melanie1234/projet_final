<?php

namespace App\Entity;
use Symfony\Component\Validator\Constraints as Assert;

class Book
{
	private ?int $id;

	#[Assert\NotBlank]
	private string $isbn13;

	private string $publishers;

	#[Assert\NotBlank]
	private string $title;

	private string $genres;

	private  $numberOfPages;

	private string $publishDate;

	#[Assert\NotBlank]
	private string $author;

	private ?string $comment;

	private ?string $img;

	private ?int $score;

	private ?int $userId;

	public function __construct(string $isbn13, string $publishers, string $title, 
	?string $genres, $numberOfPages, string $publishDate, string $author,
	 ?string $comment = null, ?string $img = null, ?int $score = null, ?int $userId = null, ?int $id=null)
	{
		$this->id = $id;
		$this->isbn13 = $isbn13;
		$this->publishers = $publishers;
		$this->title = $title;
		$this->genres = $genres;
		$this->numberOfPages = $numberOfPages;
		$this->publishDate = $publishDate;
		$this->author = $author;
		$this->comment = $comment;
		$this->img = $img;
		$this->score = $score;
		$this->userId = $userId;

	}


	/**
	 * @return 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getIsbn13(): string {
		return $this->isbn13;
	}
	
	/**
	 * @param string $isbn13 
	 * @return self
	 */
	public function setIsbn13(string $isbn13): self {
		$this->isbn13 = $isbn13;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPublishers(): string {
		return $this->publishers;
	}
	
	/**
	 * @param string $publishers 
	 * @return self
	 */
	public function setPublishers(string $publishers): self {
		$this->publishers = $publishers;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string {
		return $this->title;
	}
	
	/**
	 * @param string $title 
	 * @return self
	 */
	public function setTitle(string $title): self {
		$this->title = $title;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getGenres(): string {
		return $this->genres;
	}
	
	/**
	 * @param string $genres 
	 * @return self
	 */
	public function setGenres(string $genres): self {
		$this->genres = $genres;
		return $this;
	}
	
	public function getNumberOfPages() {
		return $this->numberOfPages;
	}

	public function setNumberOfPages( $numberOfPages): self {
		$this->numberOfPages = $numberOfPages;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPublishDate(): string {
		return $this->publishDate;
	}
	
	/**
	 * @param string $publishDate 
	 * @return self
	 */
	public function setPublishDate(string $publishDate): self {
		$this->publishDate = $publishDate;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAuthor(): string {
		return $this->author;
	}
	
	/**
	 * @param string $author 
	 * @return self
	 */
	public function setAuthor(string $author): self {
		$this->author = $author;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getComment(): string {
		return $this->comment;
	}
	
	/**
	 * @param string $comment 
	 * @return self
	 */
	public function setComment(string $comment): self {
		$this->comment = $comment;
		return $this;
	}
	
	/**
	 * @return ?string
	 */
	public function getImg(): ?string {
		return $this->img;
	}
	
	/**
	 * @param ?string $img 
	 * @return self
	 */
	public function setImg(?string $img): self {
		$this->img = $img;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getScore(): ?int {
		return $this->score;
	}
	
	/**
	 * @param int $score 
	 * @return self
	 */
	public function setScore(?int $score): self {
		$this->score = $score;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getUserId(): ?int {
		return $this->userId;
	}
	
	/**
	 * @param int $userId 
	 * @return self
	 */
	public function setUserId(?int $userId): self {
		$this->userId = $userId;
		return $this;
	}
}