<?php

namespace App\Entity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class Users implements UserInterface, PasswordAuthenticatedUserInterface {
    private ?int $id;
    #[Assert\NotBlank]
    private string $firstName;
    #[Assert\NotBlank]
    private string $lastName;
    #[Assert\NotBlank]

    private string $mail;
    #[Assert\NotBlank]

    private string $address;
    #[Assert\NotBlank]

    private string $department;
    #[Assert\NotBlank]

    private string $city;
	
    private string $role;
    #[Assert\NotBlank]

    private string $password;
    #[Assert\NotBlank]

    private int $score;
	/**
	 * @param int|null $id
	 * @param string|null $firstName
	 * @param string|null $lastName
	 * @param string|null $mail
	 * @param ?string|null $address
	 * @param ?string|null $department
	 * @param ?string|null $city
	 * @param string|null $role
 	 * @param string|null $password
	 * @param ?int|null $score
	 */

	 public function __construct(string $firstName, string $lastName, string $mail, ?string $address, ?string $department, ?string $city, string $password, int $score=0, string $role = '', ?int $id = null)
	{
		$this->id = $id;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->mail = $mail;
		$this->address = $address;
		$this->department = $department;
		$this->city = $city;
		$this->password = $password;
		$this->score = $score;
		$this->role = $role;	
	}

    /** 
	 * @param int|null 
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstName(): string {
		return $this->firstName;
	}
	
	/**
	 * @param string $firstName 
	 * @return self
	 */
	public function setFirstName(string $firstName): self {
		$this->firstName = $firstName;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getLastName(): string {
		return $this->lastName;
	}
	
	/**
	 * @param string $lastName 
	 * @return self
	 */
	public function setLastName(string $lastName): self {
		$this->lastName = $lastName;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getMail(): string {
		return $this->mail;
	}
	
	/**
	 * @param string $mail 
	 * @return self
	 */
	public function setMail(string $mail): self {
		$this->mail = $mail;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getAddress(): string {
		return $this->address;
	}
	
	/**
	 * @param string $address 
	 * @return self
	 */
	public function setAddress(string $address): self {
		$this->address = $address;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDepartment(): string {
		return $this->department;
	}
	
	/**
	 * @param string $department 
	 * @return self
	 */
	public function setDepartment(string $department): self {
		$this->department = $department;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getCity(): string {
		return $this->city;
	}
	
	/**
	 * @param string $city 
	 * @return self
	 */
	public function setCity(string $city): self {
		$this->city = $city;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getRole(): string {
		return $this->role;
	}
	
	/**
	 * @param string $role 
	 * @return self
	 */
	public function setRole(string $role): self {
		$this->role = $role;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getPassword(): string {
		return $this->password;
	}
	
	/**
	 * @param string $password 
	 * @return self
	 */
	public function setPassword(string $password): self {
		$this->password = $password;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getScore(): int {
		return $this->score;
	}
	
	/**
	 * @param int $score 
	 * @return self
	 */
	public function setScore(int $score): self {
		$this->score = $score;
		return $this;
	}

	public function getUserIdentifier():string {
        return $this->mail;
    }
    public function getRoles():array {
        return [$this->role];
    }

    public function eraseCredentials(){}
}