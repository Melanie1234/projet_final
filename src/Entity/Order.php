<?php
namespace App\Entity;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
class Order {
    private ?int $id;
    #[Assert\NotBlank]

    private ?DateTime $date;
    #[Assert\NotBlank]

    private string $deliveryMode;
    #[Assert\NotBlank]

    private string $status;
    #[Assert\NotBlank]

	private int $userId;
    #[Assert\NotBlank]

    private int $bookId;
	
	/**
	 * @param int|null $id
	 * @param DateTime|null $date
	 * @param string|null $deliveryMode
	 * @param string|null $status
	 * @param int|null $userId
	 * @param int|null $bookId
	 */
	 public function __construct(DateTime $date, string $deliveryMode,
	  string $status, ?int $userId, int $bookId, ?int $id = null)
	{
		$this->id = $id;
		$this->date = $date;
		$this->deliveryMode = $deliveryMode;
		$this->status = $status;
		$this->userId = $userId;
		$this->bookId = $bookId;
	}

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param  $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return 
	 */
	public function getDate(): ?DateTime {
		return $this->date;
	}
	
	/**
	 * @param  DateTime|null $date 
	 * @return self
	 */
	public function setDate($date): self {
		if ($date instanceof DateTime) {
			$this->date = $date;
		} elseif (is_string($date)) {
			$this->date = new DateTime($date);
		} else {
			throw new \InvalidArgumentException('Invalid date format');
		}
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDeliveryMode(): string {
		return $this->deliveryMode;
	}
	
	/**
	 * @param string $deliveryMode 
	 * @return self
	 */
	public function setDeliveryMode(string $deliveryMode): self {
		$this->deliveryMode = $deliveryMode;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}
	
	/**
	 * @param string $status 
	 * @return self
	 */
	public function setStatus(string $status): self {
		$this->status = $status;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getUserId(): int {
		return $this->userId;
	}
	
	/**
	 * @param int $userId 
	 * @return self
	 */
	public function setUserId(int $userId): self {
		$this->userId = $userId;
		return $this;
	}
	
	/**
	 * @return int
	 */
	public function getBookId(): int {
		return $this->bookId;
	}
	
	/**
	 * @param int $bookId 
	 * @return self
	 */
	public function setBookId(int $bookId): self {
		$this->bookId = $bookId;
		return $this;
	}
}