-- Active: 1673947741321@@127.0.0.1@3306@projet_final
DROP DATABASE projet_final;

CREATE DATABASE projet_final;

USE projet_final;

CREATE TABLE
    users (
        id INT PRIMARY KEY AUTO_INCREMENT,
        firstName VARCHAR(255),
        lastName VARCHAR(255),
        mail VARCHAR(255),
        address VARCHAR(255),
        department VARCHAR(255),
        city VARCHAR(255),
        role VARCHAR(255),
        password VARCHAR(255),
        score INT
    );
    
CREATE TABLE
    book (
        id INT PRIMARY KEY AUTO_INCREMENT,
        isbn_13 VARCHAR(255),
        publishers VARCHAR(255),
        title VARCHAR(255),
        genres VARCHAR(255),
        number_of_pages VARCHAR(255),
        publish_date VARCHAR(255),
        author VARCHAR(255),
        comment VARCHAR(1500),
        img VARCHAR(11000),
        score INT,
        user_id INT,
        FOREIGN KEY (user_id) REFERENCES users(id)
    );


CREATE TABLE
    `order` (
        id INT PRIMARY KEY AUTO_INCREMENT,
        date DATE,
        deliveryMode VARCHAR(255),
        status VARCHAR(255),
        user_id INT,
        FOREIGN KEY (user_id) REFERENCES users(id),
        book_id INT,
        FOREIGN KEY (book_id) REFERENCES book(id)
    );


INSERT INTO
    users (firstName, lastName, mail, address, department, city, role, password, score) VALUES (
        'ghjdghjgjf',
        'Doe',
        'john.doe@example.com',
        '123 Main St',
        'HR',
        'New York',
        'Employee',
        '$2y$13$StWlOMl6XCVz.eKzgU1bUuXHcEDbyeRhKtsnWJoQGBdM6dKRg018C',
        100
    );

    INSERT INTO book (isbn_13, publishers, title, genres, number_of_pages,
     publish_date, author, comment, img,  score, user_id)
VALUES ('9787850011849', 'Hodder and Stoughton', 'Dune', '605', '1968', 'Frank Herbert',
  'Trop long', 'sample.jpg', 5, 'SF', 1);

INSERT INTO `order` (date, deliveryMode, status, user_id, book_id)
VALUES ('2023-08-07', 'Colissimo', 'En cours', 1, 1);
